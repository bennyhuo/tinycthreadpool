# Publish to GitLab Package Registry

Add gitlab package url to conan remote:

```
conan remote add gitlab https://gitlab.com/api/v4/projects/42747603/packages/conan
```

'42747603' is the project id.

And then create the package in your local machine:

```
conan create . bennyhuo+tinycthreadpool/stable
```

This will create a package with a name 'tinycthreadpool/1.0'.

After that, upload the package:

```
conan upload tinycthreadpool/1.0@bennyhuo+tinycthreadpool/stable --all --remote=gitlab
```

Username and a personal token which is granted to api are needed.

Done. 